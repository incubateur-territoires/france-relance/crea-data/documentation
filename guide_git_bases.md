# Bases de la gestion de git

## Introduction

> __Source : [Documentation git officielle](https://git-scm.com/doc)__

Git est un logiciel permettant de gérer le versionnage d'une base de code. Il permet de faciliter le travail collaboratif et assure la conservation de l'historique de code.

Le logiciel est utilisable en ligne de commande, accessible sous tout système d'exploitation _(powershell ou cmd sous windows, bash, zsh, etc. sous linux)_

Il est possible d'utiliser git via des outils graphiques, soit dediés ([Sourcetree](https://www.sourcetreeapp.com/), [Github desktop](https://desktop.github.com/), etc.), soit integrés à un éditeur de texte ([Visual Studio Code](https://code.visualstudio.com/), [IntelliJ](https://www.jetbrains.com/idea/)).

Selon le contexte du projet et les compétences de l'équipe, l'organisation du versionnage peut très vite devenir complexe. L'équipe doit toujours veiller à opter pour un `flow` compatible avec son expertise et en accord avec l'exigence du travail Open Source.

Git fonctionne de paire avec un serveur d'hébergement de code comme [Gitlab](https://gitlab.com/) ou [Github](https://github.com/). L'objectif est de valider les changements locaux sur une machine pour pouvoir les pousser sur un dépot distant.

## Git et VSCode

Toutes les commandes relatives à git se trouvent dans le menu git situé à gauche de l'éditeur _(raccourci ctrl + shift + G)_, la suite fera donc référence à ce menu

> [Documentation git spécifique à VSCode](https://code.visualstudio.com/docs/editor/versioncontrol)

### Initialisation du projet

- Le projet n'est pas encore sur l'hébergement distant et git n'est pas initialisé : bouton `Initialize Repository`
- Il est aussi possible de cloner un dépot distant en ouvrant VSCode sans dossier ouverts: bouton `Clone Repository`

### Je fais des changements en local

A chaque sauvegarde de fichier, VSCode ajoute la liste des fichiers mis à jour dans la liste `Changes`.

__Pour voir ses changements, il suffit de cliquer sur le nom du fichier, VSCode ouvre alors deux panneaux présentant la diff__

Trois options sont possibles depuis cette liste, disponible au survol du nom du fichier :
- Ouvrir le fichier
- Annuler les changements -> Attention vous allez perdre vos changements sur le fichier concerné
- Ajouter à la liste `Stage Changes` -> Ajouter à la pile des changements en vue de `commit`

Une fois la fonctionnalité développée, on peut versionner les changements ajoutés dans la pile (aussi appelé `commmit`) pour cela il faut :
- Renseigner un message de commit dans le champ situé tout en haut du menu git de VSCode
- Valider le commit en cliquant sur l'icone `check` en haut du menu git de VSCode

__/!\ Il est recommandé d'utiliser une nomenclature commune et uniforme pour le nommage des commits, au moins sur la première ligne de son message. Il est ensuite possible d'aller à la ligne et d'ajouter des éléments plus descriptifs__

_Si vous avez ajouté des fichiers non souhaités à la pile, il est possible de cliquer sur le `-` affiche au survol du nom du fichier_

Une fois commit, votre code est versionné et les changements sont enregistrés dans l'historique git. Vous pouvez continuer à travailler en local et reprendre le processus à l'étape précédente :
- Faire des changements
- Les ajouter à la pile depuis la liste `Changes`
- Les _commit_ depuis la liste `Staged Changes`

### Je veux synchroniser mes changements locaux avec le distant

Pour pouvoir pousser son code sur un hébergement distant, il faut avoir fait le lien entre celui-ci et notre dépot local. Cette connexion s'appelle un `remote`.

Si rien n'est initialisé, cliquer sur l'icone `...` en haut du menu git de VSCode puis dans le menu contextuel `-> remote -> add remote...`

VSCode ouvre un champ en haut de son interface :
- Entrez l'URL de votre dépot distant
- Validez avec entree
- Entrez le nom de votre `remote`, s'il s'agit du premier on le nomme par convention `origin`
- Validez avec entre

__/!\ Si vous n'avez pas parametre de clé SSH, choisissez l'URL HTTP__

Si tout est bon, vous avez un icone de synchronisation qui apparait en bas à gauche de votre interface VSCode, juste à coté du nom de votre branche courante (par défaut `master` ou `main`).

Vous pouvez maintenant pousser vos changements sur le dépot distant via le menu contextuel du panneau git de VSCode : `...` -> `push` ou via le bouton `Publish Changes`

__/!\ Il est compliqué de défaire des changements poussés vers le dépot distant. Dans le cas d'erreurs ou d'oublis, il est plus simple de faire les changements en local et de repousser un nouveau `commit`.__

### S'assurer d'etre à jour du dépot distant

Avant d'entamer des changements, il est toujours préférable de `pull` l'état du dépot distant vers notre dépot local pour etre certain d'etre à jour des derniers changements. Sur VSCode il est possible d'utiliser le bouton en bas à gauche de l'interface qui permet de synchroniser les dépots entre eux (et dans les deux sens).

Il est aussi possible de `pull` via `...` -> `pull`

## Résumé d'un processus basique

- Changements sur la base de code
- Vérification des changements dans la liste `changes`
- Ajouts des changements à la pile `staged`
- Ecriture du message de commit
- Commit
- Recommencer le processus pour chaque fonctionnalité

Il est recommandé de synchroniser le distant avant et après chaque nouvelle fonctionnalité afin de ne pas trop augmenter les différences potentielles entre celui-ci et votre dépot local.

